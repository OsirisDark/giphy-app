import React from 'react';
import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';


/*const GiphList1 = [
  {id: 1, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
  {id: 2, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
  {id: 3, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
]; */

/*const GiphList2 = [
  {id: 4, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
  {id: 5, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
  {id: 6, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
]; */

/*const GiphList3 = [
  {id: 7, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
  {id: 8, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
  {id: 9, src: 'https://media.giphy.com/media/1Ahg9sI2zpz8cNEMUP/giphy.gif',},
]; */
class GiphhList extends React.Component{
   constructor(props){
      super(props);
      this.state={
         giphs: [],
         loading: true,
      };
      this.getRandomGiph = this.getRandomGiph.bind(this);
   }

   componentDidMount() {
      const url = 'http://api.giphy.com/v1/gifs/random?api_key=AKZlKwmOg9yb6ScSbAF8MhaLWQyLFj5q&limit=5';
      fetch(url).then(res => res.json()).then(response => {
         const {data} = response;
         this.setState({
            giphs: data,
            loading: false,
         })
      })
   }

   getRandomGiph(){
      const url = 'http://api.giphy.com/v1/gifs/random?api_key=AKZlKwmOg9yb6ScSbAF8MhaLWQyLFj5q';
      fetch(url).then(res => res.json()).then(jsonResponse => {
         const{data} = jsonResponse;
         console.log(data);

         this.setState({
            giphs: this.state.giphs.concat([data]),
         })
         
         /*console.log(jsonResponse);
         console.log('id', id);
         console.log('imageOriginal', image_original_url);
         console.log('status', status);*/
      } )
   }


render() {
   const {giphs} = this.state;
    return (
    <Container>
     <Carousel>
       <Carousel.Item>
          <div className = 'giph-container'>
          <div className = 'all-giphs'>
          {
            giphs.length > 0
            ? giphs.map(giph => { return <img key={giph.id} src={giph.src} />
            })
          :'No GIPHS'
          
          }
          </div>
          <button onClick={this.getRandomGiph} >AGREGAR OTRO</button> 
          </div>
         
       </Carousel.Item>
       
      { /*<Carousel.Item>
          <div className = 'giph-container'>
          
        
       </Carousel.Item>
       <Carousel.Item>
          <div className = 'giph-container'>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          </div>
        
       </Carousel.Item>
     </Carousel> <br/>

     <Carousel>
       <Carousel.Item>
          <div className = 'giph-container'>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/iLup1YcSTLaUg" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/superman-art-batman-iLup1YcSTLaUg"></a></p>
          <iframe src="https://giphy.com/embed/3Gij38skUDBvy" width="250" height="250" padding ="10"frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-robin-3Gij38skUDBvy"></a></p>         
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          </div>
        
       </Carousel.Item>
       <Carousel.Item>
          <div className = 'giph-container'>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          </div>
        
       </Carousel.Item>
       <Carousel.Item>
          <div className = 'giph-container'>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/88i6wwEpFmUJsT2wQK" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/movie-joker-joaquin-phoenix-88i6wwEpFmUJsT2wQK"></a></p>
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          <iframe src="https://giphy.com/embed/oOK9AZGnf9b0c" width="250" height="250" padding="10" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/batman-comics-oOK9AZGnf9b0c"></a></p>
          </div>
        
      </Carousel.Item>*/ }
     </Carousel> <br/>
     
    </Container>
    )
  }
}

  export default GiphhList;