import React from 'react';

function SearchBox(props){
    const { onChange, value, onSubmit } = props;
    return (
    <form onSubmit={onSubmit} >
        <input 
      className="search-input" 
      type="text" 
      placeHolder="Buscar giph" 
      value= {value} 
      onChange = {onChange}/>
      </form>
        
    );
}

export default SearchBox;