import React, { Component } from 'react';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import GiphhList from './components/Content';
import SearchBox from './components/SearchBox';


class App extends React.Component() {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    }
  }

  handleOnchange = (event) =>
    {
      this.setState({
        search: event.target.value,
      });
    }

    handleOnsubmit = (event) =>
    {
      event.preventDefault();
      console.log(this.state.search);
    }


  render(){
    return (
      <div className="App">
        <Header/>
        <Navbar />
        <SearchBox 
        value = {this.state.search}
        onChange={this.handleOnchange}
        onSubmit = {this.handleOnsubmit}
        />
        <GiphhList />
        <Footer />
      </div>  
      
    );}  
  
  }




export default App;
